import 'dart:io';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

import '../model/movie.dart';

final String key = '?api_key=469a6bfbc404b7247f5a56c48914f0a0';
final String base = 'https://api.themoviedb.org/3/movie';
final String upcoming = '/upcoming';
final String language = '&language=pt-BR';

final String urlSearchBase =
    'https://api.themoviedb.org/3/search/movie$key&query=';

class HttpHelper {
  final String url = base + upcoming + key + language;

  Future<List<Movie>> getUpcoming() async {
    var result = await http.get(url);
    List list = [];
    List<Movie> movies = [];

    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = convert.jsonDecode(result.body);
      final mapMovies = jsonResponse['results'];
      list = mapMovies.map((i) => Movie.fromJson(i)).toList();
      list.forEach((element) {
        movies.add(element);
      });
      return movies;
    }
    return movies;
  }

  Future<List<Movie>> findMovies(String title) async {
    final url = urlSearchBase + title + language;
    var result = await http.get(url);
    List list = [];
    List<Movie> movies = [];

    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = convert.jsonDecode(result.body);
      final mapMovies = jsonResponse['results'];

      list = mapMovies.map((i) => Movie.fromJson(i)).toList();
      list.forEach((element) {
        movies.add(element);
      });
    }
    return movies;
  }
}
