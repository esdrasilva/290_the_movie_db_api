import 'package:f290_the_movie_db_api/service/movie_service.dart';
import 'package:f290_the_movie_db_api/widget/widgets.dart';
import 'package:flutter/material.dart';
import 'model/movie.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String title = "";
  List movies = [];
  int itemCount = 0;
  MovieService service = MovieService();

  Icon selectIcon = Icon(Icons.search);
  Widget searchBar = Text('The MovieDB');

  @override
  void initState() {
    super.initState();
    service.getMovies(title);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: searchBar,
        actions: [
          IconButton(
            icon: selectIcon,
            onPressed: () {
              setState(() {
                if (this.selectIcon.icon == Icons.search) {
                  this.selectIcon = Icon(Icons.cancel);
                  this.searchBar = TextField(
                    onSubmitted: (text) {
                      setState(() {
                        title = text;
                      });
                    },
                    textInputAction: TextInputAction.search,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  );
                } else {
                  setState(() {
                    this.selectIcon = Icon(Icons.search);
                    this.searchBar = Text('The MovieDB');
                    title = "";
                  });
                }
              });
            },
          )
        ],
      ),
      body: _listView(),
    );
  }

  _listView() {
    Future<List<Movie>> futureListMovies = service.getMovies(title);
    return FutureBuilder(
      future: futureListMovies,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.none:
            return Center(
              child: CircularProgressIndicator(),
            );
          default:
            List<Movie> listMovie = snapshot.data;
            return ListViewMovies(listMovie);
        }
      },
    );
  }
}
