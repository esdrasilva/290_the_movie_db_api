import 'package:f290_the_movie_db_api/model/movie.dart';
import 'package:flutter/material.dart';

import '../screens/movie_detail.dart';

class ListViewMovies extends StatelessWidget {
  final List<Movie> movies;
  const ListViewMovies(this.movies);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: movies.length,
      itemBuilder: (context, index) {
        String url = _getImageUrl(movies, index);
        return Card(
          color: Colors.white,
          elevation: 4,
          child: ListTile(
            title: Text(movies[index].title),
            subtitle: Text(
                'Lançamento: ${movies[index].releaseDate} - Média: ${movies[index].voteAverage}'),
            leading: Container(
              width: 44,
              // height: 100,
              decoration: BoxDecoration(
         
                image: DecorationImage(
                    image: NetworkImage(url), fit: BoxFit.contain),
              ),
            ),
            onTap: () {
              MaterialPageRoute route = MaterialPageRoute(
                  builder: (context) => MovieDetail(movies[index]));
              Navigator.push(context, route);
            },
          ),
        );
      },
    );
  }

  String _getImageUrl(List listMovie, int index) {
    final url = listMovie[index].posterPath == null
        ? 'https://images.pexels.com/photos/2889685/pexels-photo-2889685.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'
        : 'https://image.tmdb.org/t/p/w500/${listMovie[index].posterPath}';
    return url;
  }
}
