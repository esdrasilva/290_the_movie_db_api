import 'package:f290_the_movie_db_api/helper/http_helper.dart';
import 'package:f290_the_movie_db_api/model/movie.dart';

class MovieService {
  final helper = HttpHelper();

  Future<List<Movie>> getMovies(String title) async {
    if (title.isEmpty) {
      return helper.getUpcoming();
    }
    return helper.findMovies(title);
  }
}
