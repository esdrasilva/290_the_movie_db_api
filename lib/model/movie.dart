class Movie {
  int id;
  String title;
  String overview;
  String releaseDate;
  var voteAverage;
  double popularity;
  String posterPath;
  String backdropPath;

  Movie.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        overview = json['overview'],
        releaseDate = json['release_date'],
        voteAverage = json['vote_average'],
        popularity = json['populariry'],
        posterPath = json['poster_path'],
        backdropPath = json['backdrop_path'];

  @override
  String toString() {
    return '''
    Movie{id=$id, titulo=$title, sinopse=$overview, dataLancamento=$releaseDate,
    mediaVoto=$voteAverage, popularidade=$popularity, caminhoPoster=$posterPath,
    outroPoster=$backdropPath}
    ''';
  }
}
