import 'package:f290_the_movie_db_api/model/movie.dart';
import 'package:flutter/material.dart';

class MovieDetail extends StatelessWidget {
  final Movie movie;

  final imageUrl = 'https://image.tmdb.org/t/p/w500/';

  MovieDetail(this.movie);

  get image {
    if (movie.posterPath.isEmpty) {
      return 'https://images.pexels.com/photos/2889685/pexels-photo-2889685.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500';
    }
    return '$imageUrl${movie.posterPath}';
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: Text(movie.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                child: Image.network(image),
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: Text(
                  movie.overview,
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
